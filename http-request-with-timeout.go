package main

import (
	"time"
	"fmt"
	"io/ioutil"
	"net/http"
)

func main () {
	fmt.Printf("Start\n")
	startTime := time.Now()
	client := &http.Client{
		Timeout: 11 * time.Second,
	}
	fmt.Printf("Client: %v\n", client)
	req, err := http.NewRequest("GET", "http://localhost:8000/", nil)
	resp, err := client.Do(req) 

	if err != nil {
		fmt.Println("REQ ERROR")
		fmt.Printf("Err: %v\n", err)
	} else {
		fmt.Println("OK")

		robots, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			fmt.Println("READ BODY ERROR")
		}
		fmt.Printf("Body: %s", robots)
	}
	fmt.Printf("Elapsed Time: %.3f\n", time.Now().Sub(startTime).Seconds())
}